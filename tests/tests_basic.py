"""
Below are some tests related to functions in the MRT packages
"""

import geopandas as gp
import numpy as np
import sys
from ..sample import mrt_helpers as mrt
# from sample import mrt_helpers as mrt

mrt.where_am_i()

"""
1. Testing the array2val function
"""
# all ignore values
arr1 = np.array([[0,0,0,0], [0,0,0,0], [0,0,0,0], [0,0,0,0]])
assert mrt.array2val(arr1, method='median') == np.array([[0]])
assert mrt.array2val(arr1, method='sum') == np.array([[0]])
assert mrt.array2val(arr1, method='median', ignore=0) == np.array([[0]])
assert mrt.array2val(arr1, method='henk', prio=1, th=3, ignore=0) == np.array([[0]])

#  ignore and data values
arr2 = np.array([[0,1,2,0], [0,3,4,0], [0,3,3,0], [0,4,5,0]])
assert mrt.array2val(arr2, method='median') == np.array([[0]])
assert mrt.array2val(arr2, method='median', ignore=0) == np.array([[3]])
assert mrt.array2val(arr2, method='henk', prio=4, th=2, ignore=0) == np.array([[4]])
assert mrt.array2val(arr2, method='min', ignore=0) == np.array([[1]])
assert mrt.array2val(arr2, method='max', ignore=0) == np.array([[5]])

# ignore and one other data value
arr3 = np.array([[0,1,0,0], [0,0,0,0], [0,0,0,0], [0,0,0,0]])
assert mrt.array2val(arr3, method='median') == np.array([[0]])
assert mrt.array2val(arr3, method='median', ignore=0) == np.array([[1]])
assert mrt.array2val(arr3, method='henk', prio=2, th=3, ignore=0) == np.array([[1]])
assert mrt.array2val(arr3, method='henk', prio=1, th=7, ignore=0) == np.array([[1]])
assert mrt.array2val(arr3, method='henk', prio=0, th=3, ignore=0) == np.array([[1]])

"""
2. Test reclass rules
"""
assert mrt.parse_rule("n") is None
assert mrt.parse_rule("1", "Category") == "Category in [1]"
assert mrt.parse_rule("-1", "Category") == "Category not in [1]"
assert mrt.parse_rule(1, "Category") == "Category in [1]"
assert mrt.parse_rule("1, 2, 3", "Category") == "Category in [1, 2, 3]"
assert mrt.parse_rule("-1, 2, 3", "Category") == "Category not in [1, 2, 3]"

"""
3. Extract values from rule
"""
assert mrt.extract_values('n') == None
assert mrt.extract_values('1') == [1]
assert mrt.extract_values('-1') == [1]
assert mrt.extract_values('1, 2, 3') == [1, 2, 3]
assert mrt.extract_values('-1, 2, 3') == [1, 2, 3]
assert mrt.extract_values(1) == [1]

"""
4. Write QML
"""
out_vat = gp.GeoDataFrame(data={'Value': 1, 'Description': 'test', 'geometry': None}, index=[0])
out_vat.set_geometry(col='geometry', inplace=True)
mrt.write_qml(tab=out_vat, out_dir='./tests', out_name='testqml.qml')



