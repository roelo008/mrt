"""
Command line interface for the Multi Reclass Tool (MRT).
Hans Roelofsen, WEnR, 24 augustus 2020

"""

import os
import sys
import argparse
import mrt_classes as mrtc
import mrt_helpers as mrt

parser = argparse.ArgumentParser()
parser.add_argument('xls', help='path and filename of the reclass rule XLS')
parser.add_argument('rast', help='path and filename to Combined raster', type=mrt.geospatial_raster)
parser.add_argument('us', help='Upstream: sheet name with reclass rules')
parser.add_argument('--ds', help='Downstream: sheet name with downstream categories', default=None)
parser.add_argument('--test', help='Verify that all input is in order. Do nothing', action='store_true')
parser.add_argument('--report', help='Write report to file', action='store_true')
parser.add_argument('--write', help='Name of a downstream model(s) or newval to write as raster to file', nargs='*',
                    default=[])
parser.add_argument('--xtnt', help='Extent in RD for writing raster files.', choices=['BNL', 'LCEU', 'src', 'X'],
                    nargs='*')
parser.add_argument('--of', help='Output file type.', choices=['geotiff', 'flt', 'map'], default='geotiff', type=str)
parser.add_argument('--base_out', help='Base dir for output', default=os.getcwd())
parser.add_argument('--verbose', help='verbose feedback', action='store_true')
parser.add_argument('--nodata', help='assigned nodata value for each raster to write.', nargs='*')
parser.add_argument('--mnp', help='write MNP table to file based on which model?', type=str)
parser.add_argument('--ovr', help='also build tiff pyramids', action='store_true')
args = parser.parse_args()

# TODO 27 oktober 2021. De process-flow van de MRT moet een keer opgeschoond worden. Teveel optionele argumenten etc

# Check upstream sheet
try:
    status, msg = mrt.verify_newval_description_pairing(os.path.dirname(args.xls), os.path.basename(args.xls), args.us,
                                                        downstream_sheet=args.ds)
except (AssertionError, PermissionError) as e:
    print(e)
    sys.exit(0)
print(msg)
if not status:
    sys.exit(0)

# Check downstream sheet if provided
if args.ds:
    try:
        status, msg = mrt.check_xls_downstream(os.path.dirname(args.xls), os.path.basename(args.xls), args.us, args.ds)
        print(msg)
        if not status:
            sys.exit(0)
    except (AssertionError, PermissionError) as e:
        print(e)
        sys.exit(0)

# Try to create base instance and exit cleanly if Assertion Errors arise
try:
    reclasser = mrtc.ReClass(xls_dir=os.path.dirname(args.xls),
                             xls_in=os.path.basename(args.xls),
                             upstream_sheet=args.us,
                             downstream_sheet=args.ds,
                             combined_raster_dir=os.path.dirname(args.rast),
                             combined_raster_in=os.path.basename(args.rast),
                             base_out=args.base_out,
                             verbose=args.verbose,
                             ovr=args.ovr)
except AssertionError as e:
    print(e)
    sys.exit(0)

if args.test:
    reclasser.test()
    if args.write:
        print('set to write: {}'.format(' '.join(args.write)))
    sys.exit(0)

# Parse rules and calculate reclassed values
reclasser.look_upstream()

# Parse downstream information if needed
if args.ds or any([x.startswith('_') for x in reclasser.models]):
    try:
        reclasser.look_downstream()
    except AssertionError as e:
        print(e)
        sys.exit(1)

if args.mnp:
    try:
        reclasser.mnp_table(request_raster=args.mnp)
    except (AssertionError, KeyError) as e:
        print(e)
        sys.exit(1)

# Write report if needed
if args.report or 'newval' in args.write:
    reclasser.write_report()

# Reclass input raster to one or more new rasters. Note how the extent and nodata arguments
# must be provided for each raster to write. Dit gedeelte moet nog netter georganiseerd worden.

if args.write:
    # Check for NoData arguments and parse into list of NoDatas for each raster to write
    if args.nodata:
        if len(args.nodata) != len(args.write):
            print('Provide NoData value for each raster to write, or use X for default value')
            sys.exit(1)
        nodatas = [int(v) if v != 'X' else None for v in args.nodata]
    else:
        nodatas = [None] * len(args.write)
    if args.xtnt:
        if len(args.xtnt) != len(args.write):
            print('Provide Extent option for each raster to write, or use X for default value')
            print(args.xtnt)
            sys.exit(1)
        xtnts = [v if v != 'X' else None for v in args.xtnt]
    else:
        xtnts = [None] * len(args.write)

    for rast, nodata_val, extent in zip(args.write, nodatas, xtnts):
        try:
            kwargs = {'request_raster': rast}
            if extent:  # Add extent if a Extent argument other than X is provided
                kwargs['extent'] = extent
            if args.of:
                kwargs['out_format'] = args.of
            if nodata_val != None:  # Add NoData argument if nodata argument other than X is provided
                kwargs['nodata'] = nodata_val
            reclasser.write_raster(**kwargs)
        except AssertionError as e:
            print('  Failed to write {0} due to {1}'.format(rast, e))
            continue
sys.exit(0)

