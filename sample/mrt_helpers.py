"""
Helper functions for the Multireclass functionality
Hans Roelofsen, 24 augustus 2020

"""

import os
import random
import argparse
import pandas as pd
import rasterio as rio
import numpy as np
import affine
from rasterio.io import MemoryFile
import xml.etree.ElementTree as ET
import pathlib

def vec_translate(a, my_dict):
    """
    Function to apply my_dict to each element in in array a. See:
    https://stackoverflow.com/questions/16992713/translate-every-element-in-numpy-array-according-to-key

    Note that key errors are not caught!
    """
    return np.vectorize(my_dict.__getitem__)(a)


def parse_rule(rule, category=None):
    """
    Parse rule information in reclass df to a list of values for a category a

    INPUT    OUTPUT
    1        "a in [1]"
    "1"      "a in [1]"
    "1,2,3"  "a in [1, 2, 3]"
    "-1"     "a not in [1]"
    "-1,2"   "a not in [1, 2]"
    "n"      None
    else     Exception

    :param rule: string or int
    :param category: name source raster, used to negate number(s) starting with "-". Default None
    :param negate: return negation of provided numbers if txt.startswith('-'). Default True
    :return: see above.
    """

    if isinstance(rule, int):
        vals = [rule]
        negate = False
    elif isinstance(rule, str):
        rule = rule.strip(', ')
        if rule == 'n':
            return None
        if rule.startswith('-'):
            negate = True
            rule = rule[1:]
        else:
            negate = False
        try:
            vals = [int(v.strip()) for v in rule.split(',')]
        except ValueError as e:
            raise AssertionError('Unexpected value found in reclass rule {0}: {1}'.format(rule, e))
    else:
        raise AssertionError('unexpected reclass rule found: {0}'.format(rule))
    if category:
        return '{0} {1} {2}'.format(category, 'not in' if negate else 'in', vals)
    else:
        raise AssertionError('Provide valid category')


def extract_values(rule):
    """
    Strip the numerical values from a reclass rule component
    :param rule: see below
    :return: see below

    INPUT    OUTPUT
    1        [1]
    "1"      [1]
    "1,2,3"  [1, 2, 3]
    "-1"     [1]
    "-1,2"   [1, 2]
    "n"      None
    else     Exception
    """

    if isinstance(rule, int):
        return [rule]
    elif isinstance(rule, str):
        rule = rule.strip(', ')
        if rule == 'n':
            return None
        elif rule.startswith('-'):
            rule = rule[1:]
        else:
            rule = rule
        try:
            return [int(v.strip()) for v in rule.split(',')]
        except ValueError:
            print('unexpected reclass rule found: {0}'.format(rule))
            raise
    else:
        raise Exception('unexpected reclass rule found: {0}'.format(rule))


def mnp_abiotiek_raster_profile():
    """
    return raster profile for MNP abiotische rasters 
    """

    prof = rio.default_gtiff_profile
    prof.update(width=11200, 
                height=13000,
                dtype=rio.dtypes.float32,
                driver='GTiff',
                transform=affine.Affine(25, 0.0, 0, 0.0, -25.0, 625000.0),
                count=1,
                nodata=-9999.0,
                crs=rio.crs.CRS.from_epsg(28992))
    return prof
                


def raster_profile(extent, pxl_size, **kwargs):
    """
    Return rasterio raster profiles
    :param extent: extent in RD-New. Either LCEU (10.000, 300.000, 280.000, 620.000)
                                            BNL (0, 300.000, 280.000, 625.000)
    :pxl_size: either 10 or 2.5 m.
    :return:raster profile
    """

    default = rio.default_gtiff_profile
    default.update(width=27000,
                   nodata=255.0,
                   blockxsize=128,
                   blockysize=128,
                   crs=rio.crs.CRS.from_epsg(28992),
                   height=32000,
                   transform=affine.Affine(10.0, 0.0, 0, 0.0, -10.0, 625000.0),
                   bigtiff='NO',
                   count=1)

    if extent == 'LCEU' and pxl_size == 2.5:
        default.update(transform=affine.Affine(2.5, 0, 10000, 0, -2.5, 620000),
                       width=108000, height=128000)
    elif extent == 'LCEU' and pxl_size == 10:
        default.update(transform=affine.Affine(10, 0, 10000, 0, -10, 620000),
                       width=27000, height=32000)
    elif extent == 'BNL' and pxl_size == 2.5:
        default.update(transform=affine.Affine(2.5, 0, 0, 0, -2.5, 625000),
                       width=112000, height=130000)
    elif extent == 'BNL' and pxl_size == 10:
        default.update(transform=affine.Affine(10, 0, 0, 0, -10, 625000),
                       width=28000, height=32500)
    elif extent == 'src':
        try:
            src = kwargs.get('src')
            default.update(transform=src.transform, width=src.width, height=src.height)
        except AttributeError as e:
            print('please provide source raster profile to mimic')
            raise AssertionError

    else:
        raise Exception('Not a valid raster profile request')
    return default


def format2ext(raster_format):
    """
    Return file extension corresponding to requested geospatial raster file format
    :param raster_format: requested format {geotiff, flt, map}
    :return: {tiff, flt, map}
    """
    driver2ext = {'geotiff': 'tif', 'flt': 'flt', 'map': 'map', 'geotif': 'tif', 'tiff': 'tif', 'tif': 'tif'}
    try:
        return driver2ext[raster_format]
    except KeyError:
        raise Exception('Invalid format: {}'.format(raster_format))


def format2driver(raster_format):
    """
    Return gdal file driver corresponding to requested geospatial raster file format
    :param raster_format: requested format {geotiff, flt, map}
    :return: {GTiff, EHdr, PCRaster}
    """
    format2driver = {'geotiff': 'GTiff', 'flt': 'EHdr', 'map': 'PCRaster'}
    try:
        return format2driver[raster_format]
    except KeyError:
        raise Exception('Invalid format: {}'.format(raster_format))


def write_vat(gdf, out_dir, out_name, v):
    """
    Write a geodataframe to file as shapefile, then remove all but the DBF file so that the DBF serves as raster VAT
    :param gdf: geopandas geodataframe with columns:
                Descriptio(n): <Beheertypecode> <Beheertype omschrijving>
                Value:         <pixel value>
    :param out_dir: output dir
    :param out_name: outputname without extension
    :param v: verbose, booelean
    :return: <output_dir>/<output_name>.tif.vat.dbf
    """

    out_file = os.path.join(out_dir, '{0}.shp'.format(out_name))
    gdf.to_file(out_file)

    # delete all shapefile components except dbf
    redundant_shp_files = [f for f in os.listdir(out_dir) if \
                           os.path.splitext(f)[1] in ['.shp', '.shx', '.prj', '.sbn', '.sbx', '.cpg', 'shp.xml']]
    for f in redundant_shp_files:
        os.remove(os.path.join(out_dir, f))

    # rename shapefile DBF for ArcGIS Pro to recognize it as part of the TIFF
    old_name = '{0}.dbf'.format(out_name)
    new_name = '{0}.tif.vat.dbf'.format(out_name)
    try:
        os.rename(os.path.join(out_dir, old_name), os.path.join(out_dir, new_name))
    except FileExistsError:
        print('huh, {} already exists...'.format(new_name))
    if os.path.isfile(os.path.join(out_dir, new_name)) and v:
        print('Created raster VAT {}'.format(new_name))


def progress_bar(iterable, prefix='', suffix='', decimals=1, length=100, fill='█', printEnd="\r"):
    """
    Call in a loop to create terminal progress bar
    @params:
        iteration   - Required  : current iteration (Int)
        total       - Required  : total iterations (Int)
        prefix      - Optional  : prefix string (Str)
        suffix      - Optional  : suffix string (Str)
        decimals    - Optional  : positive number of decimals in percent complete (Int)
        length      - Optional  : character length of bar (Int)
        fill        - Optional  : bar fill character (Str)
        printEnd    - Optional  : end character (e.g. "\r", "\r\n") (Str)
    credits: https://stackoverflow.com/questions/3173320/text-progress-bar-in-the-console
    """
    total = len(iterable)

    def printProgressBar(iteration):
        percent = ("{0:." + str(decimals) + "f}").format(100 * (iteration / float(total)))
        filledLength = int(length * iteration // total)
        bar = fill * filledLength + '-' * (length - filledLength)
        print('\r{0} |{1}| {2}% {3}'.format(prefix, bar, percent, suffix), end=printEnd)

    # Initial Call
    printProgressBar(0)

    # Update Progress Bar
    for i, item in enumerate(iterable):
        yield item
        printProgressBar(i + 1)
    # Print New Line on Complete
    print()


def geospatial_raster(x):
    """
    Is x a geospatial raster?
    :param x: file path
    :return: x if True else argparse.ArgumentTypeError
    """

    if os.path.isfile(x) or os.path.isdir(x):
        try:
            rast = rio.open(x)
            return x
        except(rio.errors.RasterioIOError, rio.errors.FileError, rio._err.CPLE_OpenFailed) as e:
            raise argparse.ArgumentTypeError(e)
    else:
        raise argparse.ArgumentTypeError('Not a file: {0}'.format(x))


def rand_web_color_hex():
    """"
    Generate random color.
    https://blog.stigok.com/2019/06/22/generate-random-color-codes-python.html
    """
    rgb = ""
    for _ in "RGB":
        i = random.randrange(0, 2**8)
        rgb += i.to_bytes(1, "big").hex()
    return '#{}'.format(rgb)


def bdb_legenda_rogier(cat):
    """
    Return HEX color code and Alpha code for NVK BDB category as defined by Rogier Pouwels in
    bdb2050_t5-20210127-204901_newval.qml
    :param cat: land use category
    :return: assigned HEX color for cat if cat is known, else random color
             assigned alpha value, else 255
    """

    cat2color = {"N01.01 Zee en Wad": {'color':"#2133e7", 'alpha':"51"},
                 "N02.01 Rivier": {'color':"#2133e7", 'alpha':"51"},
                 "N03.01 Beek en bron": {'color':"#2133e7", 'alpha':"51"},
                 "N04.01 Kranswierwater": {'color':"#2133e7", 'alpha':"51"},
                 "N04.02 Zoete plas": {'color':"#2133e7", 'alpha':"175"},
                 "N04.03 Brak water": {'color':"#2133e7", 'alpha':"51"},
                 "N04.04 Afgesloten zeearm": {'color':"#2133e7", 'alpha':"51"},
                 "N05.01 Moeras": {'color':"#b4ac17", 'alpha':"255"},
                 "N05.01.02 Landriet": {'color':"#b4ac17", 'alpha':"255"},
                 "N05.01.03 Waterriet": {'color':"#b4ac17", 'alpha':"255"},
                 "N05.01.06 Moerasstruweel": {'color':"#b4ac17", 'alpha':"255"},
                 "N05.01.11 Galigaanmoerassen": {'color':"#b4ac17", 'alpha':"255"},
                 "N05.01.13 Open zand": {'color':"#d7d01c", 'alpha':"255"},
                 "N05.01.14 Slikkige rivieroever": {'color':"#d7d01c", 'alpha':"255"},
                 "N05.02 Gemaaid rietland": {'color':"#b4ac17", 'alpha':"255"},
                 "N06.01 Veenmosrietland en moerasheide": {'color':"#8054cc", 'alpha':"255"},
                 "N06.02 Trilveen": {'color':"#b4ac17", 'alpha':"255"},
                 "N06.03 Hoogveen": {'color':"#672ccc", 'alpha':"255"},
                 "N06.04 Vochtige heide": {'color':"#b154cc", 'alpha':"255"},
                 "N06.05 Zwakgebufferd ven": {'color':"#2133e7", 'alpha':"51"},
                 "N06.06 Zuur ven of hoogveenven": {'color':"#2133e7", 'alpha':"51"},
                 "N07.01 Droge heide": {'color':"#8054cc", 'alpha':"255"},
                 "N07.02 Zandverstuiving": {'color':"#eae01e", 'alpha':"255"},
                 "N08.01 Strand en embryonaal duin": {'color':"#e6dc1d", 'alpha':"255"},
                 "N08.02.00 Open duin": {'color':"#d7d01c", 'alpha':"255"},
                 "N08.02.02 Stuivend duinzand": {'color':"#ebe11e", 'alpha':"255"},
                 "N08.02.03 Witte duinen": {'color':"#d7d01c", 'alpha':"255"},
                 "N08.02.07 Droog duingrasland kalkrijk": {'color':"#88d667", 'alpha':"255"},
                 "N08.02.08 Drooog duingrasland kalkarm": {'color':"#88d667", 'alpha':"255"},
                 "N08.02.11 Duinstruweel": {'color':"#88d667", 'alpha':"255"},
                 "N08.02.15 Duingrasland": {'color':"#88d667", 'alpha':"255"},
                 "N08.03 Vochtige duinvallei": {'color':"#b4ac17", 'alpha':"255"},
                 "N08.04 Duinheide": {'color':"#8054cc", 'alpha':"255"},
                 "N09.01 Schor of kwelder": {'color':"#1928b1", 'alpha':"137"},
                 "N10.01 Nat schraalland": {'color':"#33cc28", 'alpha':"255"},
                 "N10.02 Vochtig hooiland": {'color':"#33cc28", 'alpha':"255"},
                 "N11.01 Droog schraalgrasland": {'color':"#33cc28", 'alpha':"255"},
                 "N12.01 Bloemdijk": {'color':"#33cc28", 'alpha':"255"},
                 "N12.02 Kruiden- en faunarijk grasland": {'color':"#b8cc38", 'alpha':"255"},
                 "N12.03 Glanshaverhooiland": {'color':"#33cc28", 'alpha':"255"},
                 "N12.04 Zilt- en overstromingsgrasland": {'color':"#33cc28", 'alpha':"255"},
                 "N12.05 Kruiden- en faunrarijke akker": {'color':"#cba38c", 'alpha':"255"},
                 "N12.05 Kruiden- en faunarijke akker": {'color':"#cba38c", 'alpha':"255"},
                 "N12.06 Ruigteveld": {'color':"#42cc38", 'alpha':"255"},
                 "N13.01 Vochtig weidevogelgrasland": {'color':"#33cc28", 'alpha':"255"},
                 "N13.02 Wintergastenweide": {'color':"#33cc28", 'alpha':"255"},
                 "nietBA_plaagongevoellig gras (maaien)": {'color':"#33cc28", 'alpha':"255"},
                 "N14.01 Rivier- en beekbegeleidend bos": {'color':"#298023", 'alpha':"255"},
                 "N14.02 Hoog- en laagveenbos": {'color':"#298023", 'alpha':"255"},
                 "N14.03 Haagbeuken- en essenbos": {'color':"#298023", 'alpha':"255"},
                 "N15.01.01 Duinbos -- gemengd bos": {'color':"#298023", 'alpha':"255"},
                 "N15.01.02 Duinbos -- loofbos": {'color':"#298023", 'alpha':"255"},
                 "N15.01.03 Duinbos -- naaldbos": {'color':"#1f611a", 'alpha':"255"},
                 "N15.01.00 Duinbos": {'color':"#298023", 'alpha':"255"},
                 "N15.01 Duinbos": {'color':"#298023", 'alpha':"255"},
                 "N15.02.01 Dennen-, eiken-, en beukenbos -- gemengd bos": {'color':"#298023", 'alpha':"255"},
                 "N15.02.02 Dennen-, eiken-, en beukenbos -- loofbos": {'color':"#298023", 'alpha':"255"},
                 "N15.02.03 Dennen-, eiken-, en beukenbos -- naaldbos": {'color':"#21661c", 'alpha':"255"},
                 "N15.02.00 Dennen-, eiken-, en beukenbos ": {'color':"#298023", 'alpha':"255"},
                 "N15.02 Dennen-, eiken-, en beukenbos": {'color':"#298023", 'alpha':"255"},
                 "N16.01.01 Droog bos met productie (vervallen) -- gemengd bos": {'color':"#298023", 'alpha':"255"},
                 "N16.01.02 Droog bos met productie (vervallen) -- loofbos": {'color':"#298023", 'alpha':"255"},
                 "N16.01.03 Droog bos met productie (vervallen) -- naaldbos": {'color':"#1b5617", 'alpha':"255"},
                 "N16.01.00 Droog bos met productie (vervallen)": {'color':"#298023", 'alpha':"255"},
                 "N16.01 Droog bos met productie (vervallen)": {'color':"#298023", 'alpha':"255"},
                 "N16.02.00 Vochtig bos met productie (vervallen)": {'color':"#298023", 'alpha':"255"},
                 "N16.02 Vochtig bos met productie (vervallen)": {'color':"#298023", 'alpha':"255"},
                 "N16.03.01 Droog bos met productie -- gemengd bos": {'color':"#298023", 'alpha':"255"},
                 "N16.03.02 Droog bos met productie -- loofbos": {'color':"#298023", 'alpha':"255"},
                 "N16.03.03 Droog bos met productie -- naaldbos": {'color':"#1f611a", 'alpha':"255"},
                 "N16.03.00 Droog bos met productie": {'color':"#298023", 'alpha':"255"},
                 "N16.03 Droog bos met productie": {'color':"#9c2f30", 'alpha':"255"},
                 "N16.04.01 Vochtig bos met productie -- gemengd bos": {'color':"#298023", 'alpha':"255"},
                 "N16.04.02 Vochtig bos met productie -- loofbos": {'color':"#298023", 'alpha':"255"},
                 "N16.04.03 Vochtig bos met productie -- naaldbos": {'color':"#1c5918", 'alpha':"255"},
                 "N16.04.00 Vochtig bos met productie": {'color':"#298023", 'alpha':"255"},
                 "N16.04 Vochtig bos met productie": {'color':"#c75a93", 'alpha':"255"},
                 "N17.01.00 Vochtig hakhout en middenbos (vervallen)": {'color':"#298023", 'alpha':"255"},
                 "N17.02 Drooghakhout": {'color':"#d3e018", 'alpha':"255"},
                 "N17.03 Park- en stinzenbos": {'color':"#cad141", 'alpha':"255"},
                 "N17.04 Eendenkooi": {'color':"#2133e7", 'alpha':"52"},
                 "N17.05 Wilgengriend": {'color':"#7c8036", 'alpha':"255"},
                 "N17.06 Vochtig en hellinghakhout": {'color':"#d6d8ba", 'alpha':"255"},
                 "groen dak": {'color':"#d4eb97", 'alpha':"255"},
                 "Waterloop lijnvormig in de stad": {'color':"#2133e7", 'alpha':"51"},
                 "Bomenrij in de stad": {'color':"#6d9553", 'alpha':"255"},
                 "Zee": {'color':"#2133e7", 'alpha':"51"},
                 "Waterloop, meer, plas, evt met riet, in de stad": {'color':"#2133e7", 'alpha':"51"},
                 "Gebouw in de stad": {'color':"#b31159", 'alpha':"51"},
                 "Gebouw in de stad met groen dak": {'color':"#992157", 'alpha':"51"},
                 "Gebouw op bedrijventerrein": {'color':"#b31159", 'alpha':"51"},
                 "Gebouw op bedrijventerrein met groen dak": {'color':"#992157", 'alpha':"51"},
                 "Gebouw in buitengebied": {'color':"#b31159", 'alpha':"51"},
                 "Infrastructuur in de stad": {'color':"#000000", 'alpha':"51"},
                 "parkeerplaats in de stad": {'color':"#000000", 'alpha':"51"},
                 "nieuwe duurzame stad": {'color':"#b31159", 'alpha':"128"},
                 "Meer, plas (evt met riet)": {'color':"#2133e7", 'alpha':"51"},
                 "Waterloop, evt met riet, in buitengebied": {'color':"#2133e7", 'alpha':"51"},
                 "Autosnelweg": {'color':"#000000", 'alpha':"51"},
                 "Hoofdweg": {'color':"#000000", 'alpha':"51"},
                 "Regionale weg": {'color':"#000000", 'alpha':"50"},
                 "Lokale weg": {'color':"#000000", 'alpha':"52"},
                 "Straat": {'color':"#000000", 'alpha':"51"},
                 "Overige infrastructuur, half- of geheel onverhard": {'color':"#000000", 'alpha':"52"},
                 "Infrastructuur, langzaam verkeer": {'color':"#000000", 'alpha':"51"},
                 "parkeerplaats": {'color':"#000000", 'alpha':"51"},
                 "Overige infrastructuur": {'color':"#000000", 'alpha':"51"},
                 "Spoorbaanlichaam (op brug)": {'color':"#000000", 'alpha':"51"},
                 "Kas": {'color':"#b31159", 'alpha':"51"},
                 "Bomenrij waterloop en infrastructuur in buitengebied": {'color':"#298023", 'alpha':"255"},
                 "Infrastructuur en waterloop in buitengebied": {'color':"#000000", 'alpha':"52"},
                 "Bomenrij en infrastructuur in buitengebied": {'color':"#004500", 'alpha':"50"},
                 "Bomenrij en waterloop in buitengebied": {'color':"#265808", 'alpha':"51"},
                 "Waterloop lijnvormig in buitengebied": {'color':"#2133e7", 'alpha':"51"},
                 "Bomenrij  in buitengebied": {'color':"#265808", 'alpha':"52"},
                 "heg, haag in buitengebied": {'color':"#265808", 'alpha':"51"},
                 "heg, haag in de stad": {'color':"#88d667", 'alpha':"128"},
                 "agrarische bosjes en heggen": {'color':"#265808", 'alpha':"52"},
                 "nietBA_plaaggevoellig pootaardappelen": {'color':"#eb520b", 'alpha':"255"},
                 "nietBA_plaaggevoellig consumptieaardappelen": {'color':"#19dfb1", 'alpha':"255"},
                 "nietBA_plaaggevoellig tulp": {'color':"#84520b", 'alpha':"255"},
                 "welBA_plaaggevoellig appel_peer_fruit": {'color':"#84520b", 'alpha':"127"},
                 "nietBA_plaaggevoellig overige gewassen": {'color':"#dcfdd0", 'alpha':"255"},
                 "nietBA_plaaggevoellig snijmais": {'color':"#d7d01c", 'alpha':"127"},
                 "nietBA_plaaggevoellig suikerbieten": {'color':"#52d61a", 'alpha':"128"},
                 "nietBA_plaaggevoellig wintertarwe": {'color':"#b4ac17", 'alpha':"127"},
                 "nietBA_plaaggevoellig zaaiuien": {'color':"#33a02c", 'alpha':"128"},
                 "Loofbos": {'color':"#38800c", 'alpha':"255"},
                 "Akkerrand": {'color':"#84520b", 'alpha':"26"},
                 "Natuurvriendelijke oever": {'color':"#52d61a", 'alpha':"127"},
                 "Weidevogelbeheer": {'color':"#88d667", 'alpha':"255"},
                 "Grasland, evt met riet": {'color':"#88d667", 'alpha':"255"},
                 "Bouwland": {'color':"#84520b", 'alpha':"255"},
                 "Boomgaard": {'color':"#298023", 'alpha':"255"},
                 "Fruitkwekerij": {'color':"#298023", 'alpha':"255"},
                 "Boomkwekerij": {'color':"#42cc38", 'alpha':"255"},
                 "Braakliggend evt met riet": {'color':"#84520b", 'alpha':"26"},
                 "Bos op dodenakker": {'color':"#298023", 'alpha':"255"},
                 "Gemengd bos": {'color':"#298023", 'alpha':"255"},
                 "Naaldbos": {'color':"#265808", 'alpha':"255"},
                 "Populierenbos": {'color':"#298023", 'alpha':"255"},
                 "Griend": {'color':"#298023", 'alpha':"255"},
                 "Zand, evt met riet": {'color':"#d7d01c", 'alpha':"255"},
                 "Duin, evt met riet": {'color':"#d7d01c", 'alpha':"255"},
                 "Heide, evt met riet": {'color':"#8054cc", 'alpha':"255"},
                 "Bomen op overig grondgebruik in buitengebied": {'color':"#298023", 'alpha':"255"},
                 "Overige bomen op overig grondgebruik in stedelijk gebied": {'color':"#298023", 'alpha':"255"},
                 "Overig grondgebruik, evt met riet": {'color':"#000000", 'alpha':"128"},
                 "Bedrijventerrein": {'color':"#b31159", 'alpha':"51"},
                 "No other rule applies": {'color':"#2133e7", 'alpha':"0"},
                 "Overig grondgebruik in stedelijk gebied": {'color': "#e9e9e9", 'alpha':"255"},
                 "zonnepaneel": {'color': "#5b5b5b", 'alpha':"255"}}
    try:
        return cat2color[cat]['color'], cat2color[cat]['alpha']
    except KeyError:
        return rand_web_color_hex(), "255"


def write_qml(tab, out_dir, out_name):
    """
    Write QGIS QML file
    :param tab: pandas dataframe with 2 columns:
                Description: <Beheertypecode> <Beheertype omschrijving> eg N07.02 Zandverstuiving
                Value:            <pixel value>
    :param out_dir:
    :param out_name:
    :return:
    """
    try:
        filepath = pathlib.Path(__file__)
        mrt_dir = str(pathlib.Path(filepath).parents[1])  # this is the directory one level up from the 'sample' directory containing this file.
        tree = ET.parse(os.path.join(mrt_dir, 'resources/bdb_huidig_qgis_style_file.qml'))  # eg read from <mrt_dir>/resources/
    except (FileNotFoundError, PermissionError):
        print('Failed to write QML file, cannot find template')
        raise

    root = tree.getroot()
    target = root[2][0][2]
    assert target.tag == 'colorPalette'
    for x in target.findall('paletteEntry'):
        target.remove(x)

    tab.sort_values(by='Value', inplace=True)
    vals = tab.Value
    desc = tab.Description
    for v, d in zip(vals, desc):
        col, alpha = bdb_legenda_rogier(d)
        ET.SubElement(target, 'paletteEntry', attrib={'color': col, 'value': str(v),
                                                      'label': '{0}: {1}'.format(v, d), 'alpha': alpha})
    tree.write(os.path.join(out_dir, '{}.qml'.format(out_name)))


def create_rio(prof, arr):
    """
    Create a rasterio Dataset Reader object in memory.
    See: https://gis.stackexchange.com/questions/329434/creating-an-in-memory-rasterio-dataset-from-numpy-array
    :param prof: rasterio Dataset profile
    :param arr: numpy array
    :return: rasterio.io.DataSetReader object
    """
    with MemoryFile() as memfile:
        # memfile = MemoryFile()
        dataset = memfile.open(**prof)
        dataset.write(arr, indexes=1)
        return dataset


def verify_newval_description_pairing(xls_dir, xls_name, upstream_sheet='upstream_250cm', **kwargs):
    """
    Verify that the decision rules in the input XLS are valid.
    Ie each newval must be coupled 1:1 to a Description so that:

      len(set([p for p in zip(newval, Description)])) == len(set(newvals)) == len(set(Description))

    Note that in the upstream sheet, newval-description pairs may be used more than once, because different rules
    can lead to the same outcome

    :param xls_dir: directory
    :param xls_name: xls name
    :param upstream_sheet: name with upstream rules
    :return: Boolean, message
    """

    status = True
    msg = ''

    # Read Excel file
    upstream = pd.read_excel(os.path.join(xls_dir, xls_name), sheet_name=upstream_sheet)
    pairs = set([p for p in zip(upstream.newval, upstream.Description)])

    if kwargs.get('downstream_sheet'):
        downstream = pd.read_excel(os.path.join(xls_dir, xls_name), sheet_name=kwargs.get('downstream_sheet'))
        pairs = pairs.union(set([p for p in zip(downstream.newval, downstream.Description)]))

    newvals = pd.Series([i for i, _ in pairs]).value_counts()
    descriptions = pd.Series([j for _, j in pairs]).value_counts()

    if any(newvals > 1):
        duplicates = newvals[newvals.index[newvals > 1]]
        sel_pairs = [(i, j) for i, j in pairs if i in duplicates]
        status = False
        msg += 'The following newvals are coupled to different descriptions:\n{}'.format('\n'.join(['{0}: {1}'.format(i,j) for i,j in sel_pairs]))
    elif any(descriptions > 1):
        duplicates = descriptions[descriptions.index[descriptions > 1]]
        sel_pairs = [(i, j) for i, j in pairs if j in duplicates]
        status = False
        msg += '\n\nThe following descriptions are coupled to different newvals:\n{}'.format('\n'.join(['{0}: {1}'.format(i,j) for i,j in sel_pairs]))
    else:
        msg = 'Valid pairings of newval-categories.'
    return status, msg


def check_xls_downstream(xls_dir, xls_name, us, ds, strict=False):
    """
    Verify that newval-description pairs are identical between the upstream and downstream sheets
    :param xls_dir: directory
    :param xls_name: xls name
    :param us: upstream sheet name
    :param ds: downstream sheet name
    :param strict: If False downstream sheet may contains newval-category pairs that are not mentioned in upstream.
                   If True, downstream newval-category pairs must be subset of upstream pairs
    :return: Boolean, message
    """
    status = True
    msg = ''

    try:
        us = pd.read_excel(os.path.join(xls_dir, xls_name), us)
        ds = pd.read_excel(os.path.join(xls_dir, xls_name), ds)
    except (ValueError, PermissionError) as e:
        raise AssertionError('Cannot open {0}'.format(os.path.join(xls_dir, xls_name)))
    us_pairs = set([p for p in zip(us.newval, us.Description)])
    ds_pairs = set([p for p in zip(ds.newval, ds.Description)])

    if us_pairs.difference(ds_pairs):
        msg += 'Error: upstream pairs are not mentioned Downstream: {0}\n'.format('\n'.join([f'{a}-{b}' for a,b in us_pairs.difference(ds_pairs)]))
        status = False

    if ds_pairs.difference(us_pairs):
        msg += 'Warning, downstream sheet contains newval-descriptions pair(s) outside upstream sheet!\n'

    else:
        msg += 'Downstream sheet in orde.'

    return status, msg


def rule999(src_rasters):
    """
    Create rule999 to catch all values in self.combi_raster.rat. See also:
    # https://stackoverflow.com/questions/46822423/pandas-dataframe-query-expression-that-returns-all-rows-by-default
    :param src_rasters: list of source rasters
    :return: pd.Series with all attributes to attach to self.rules_df and bdb_query that applies to all rows in
             self.combi_raster.rat
    """
    data_p1 = dict.fromkeys(src_rasters, 'n')
    data_p2 = {'newval': 0, 'reclass': 1, 'description': 'No other rule applies', 'mrt_query': 'index == index',
               'remark': 'no other rule applies'}
    dummy_rule = pd.Series(data={**data_p1, **data_p2}, name=999)
    return dummy_rule


def dtype2nodata(dtype: np.dtype):
    """
    Find appropriate nodata value for a dtype
    :param dtype: dtype
    :return:
    """

    assert isinstance(dtype, np.dtype)

    d = {'f': lambda x: np.finfo(x).max,
         'i': lambda x: np.iinfo(x).max,
         'u': lambda x: np.iinfo(x).max}
    return d[dtype.name[0]](dtype)


def vals2dtype(vals):
    """
    Find appropriate numpy datatype for hosting everything between min and max values
    :param vals: list of values
    :return: numpy datatype
    """

    rio_dtype = rio.dtypes.get_minimum_dtype(vals)
    return np.dtype(rio_dtype)


def raise_assertion_error(msg):
    raise AssertionError(msg)

