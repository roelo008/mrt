"""
Tool to pad a geospatial raster with its NoData value to meet the extent of BNL. Assumes the raster already has
2.5m resolution and raster extent fully lies within the BNL extent.

BNL target profile
EXTENT: 0, 300.000, 280.000, 625.000 (left, bottom, right, top)
SHAPE: 112.000 * 130.000 (nrow x ncol)
RESOlUTION: 2.5m

Arguments
:param src: path to geospatial raster with extent within target extent but resolution = 2.5
:param dst: path to output raster to be created.
"""

import argparse
import rasterio as rio
import affine
import numpy as np
import mrt_helpers as mrt
import datetime
import os


def bnl_padding(source, destination, tr, xtnt='LCEU'):
    """
    Pad a geospatial raster with additional lines and columns to meet a predifined spatial extent.
    :param source: path to geospatial raster file or a rasterio object
    :param destination: path and filename of destination file
    :param tr: target resolution in m. Must equal resolution of source
    :param xtnt: target extent. BNL       0, 300.000, 280.000, 625.000 (left,  bottom, right, top, meter in RDNew)
                                LCEU 10.000, 300.000, 280.000, 620.000
    :return: geospatial raster on file with identical content as source, padded with source NoData value to meet
             spatial extent 0, 300.000, 280.000, 625.000 (left, bottom, right, top) in proj-crs EPSG 28992
    Source extent must be within destination extent.
    """

    # Timestamp
    ts = datetime.datetime.now().strftime('%d-%b-%Y_%H:%M:%S')

    # Destination image
    dest_bounds = {'BNL': rio.coords.BoundingBox(left=0, bottom=300000, right=280000, top=625000),
                   'LCEU': rio.coords.BoundingBox(left=10000, bottom=300000, right=280000, top=620000)}[xtnt]

    dest_affine = affine.Affine(tr, 0.0, dest_bounds.left, 0.0, -tr, dest_bounds.top)
    width_m = np.subtract(dest_bounds.right, dest_bounds.left)
    height_m = np.subtract(dest_bounds.top, dest_bounds.bottom)
    width_pxls, width_remain = np.divmod(width_m, tr)
    height_pxls, height_remain = np.divmod(height_m, tr)
    assert width_remain == 0, 'Width of {0}m is not compatible with pixels size {1}'.format(width_m, tr)
    assert height_remain == 0, 'Height of {0}m is not compatible with pixels size {1}'.format(height_m, tr)

    # Get source image and determine its origin within the destination image.
    if isinstance(source, str):
        if mrt.geospatial_raster(source):
            src = rio.open(source)
    elif isinstance(source, (rio.io.DatasetReader, rio.io.DatasetWriter)):
        src = source
    else:
        raise Exception('Provide either a rasterio DatasetReader or path to a geospatial raster.')
    coll_offset, row_offset = ~dest_affine * (src.bounds.left, src.bounds.top)

    # Check is src is properly within dest.bounds
    if rio.coords.disjoint_bounds(dest_bounds, src.bounds):
        raise Exception('Source and destination bounds are disjoint')
    assert src.res == (tr, tr), 'Source does not match target resolution.'
    assert coll_offset + src.width <= width_pxls, 'source lies (partly) outside target on E-W axis '
    assert row_offset + src.height <= height_pxls, 'source lies (partly) outside target on N-S axis '
    assert os.path.isdir(os.path.dirname(destination)), 'Invalid directory: {}'.format(os.path.dirname(destination))

    # Build output profile
    prof = src.profile
    prof.update(width=width_pxls, height=height_pxls, nodata=src.nodata, bigtiff='IF_NEEDED', transform=dest_affine,
                compress='LZW', crs=rio.crs.CRS.from_epsg(28992))

    # Single row of src NoData values to write
    if hasattr(src, 'nodata'):
        write_val = src.nodata
    else:
        write_val = 0

    # Dummy line to write
    dummyline = np.array([write_val] * width_pxls, dtype=prof['dtype']).reshape((1, width_pxls))

    with rio.open(destination, 'w', **prof) as dest:
        dest.update_tags(creation_date=ts, created_by=os.getenv('USERNAME'), src_file=source,
                         remark='padded to {} extent w. PAD2BNL.py'.format(xtnt))

        # Write destination image line by line.
        for line in mrt.progress_bar(iterable=[i for i in range(0, height_pxls)], prefix='2BNL', suffix='♡', length=50):

            # Get row offset of current block
            write_window = rio.windows.Window(col_off=0, row_off=line, width=width_pxls, height=1)
            block_row_offset = write_window.row_off

            # If row offset is within the extent of source image within the target image, then update data2write
            if row_offset <= block_row_offset < row_offset + src.height:

                # Read which line from source image?
                read_row_offset = block_row_offset - row_offset
                src_read_window = rio.windows.Window(col_off=0, row_off=read_row_offset, width=src.width, height=1)

                # Read appropriate line from the source image
                src_data = src.read(1, window=src_read_window)

                # Plug data from source into data2write
                write_col_start = int(coll_offset)
                write_col_end = int(coll_offset + src.width)
                data2write = dummyline
                data2write[:, write_col_start: write_col_end] = src_data
            else:
                data2write = dummyline

            # Write to destination
            dest.write(data2write, window=write_window, indexes=1)


if __name__ == '__main__':

    parser = argparse.ArgumentParser()
    parser.add_argument('src', help='path and filename to source raster', type=mrt.geospatial_raster)
    parser.add_argument('dst', help='path and filename to output raster')
    parser.add_argument('tr', help='target resolution', type=int)
    parser.add_argument('xtnt', help='target resolution', choices=['BNL', 'LCEU'])
    args = parser.parse_args()

    bnl_padding(source=args.src, destination=args.dst, tr=args.tr, xtnt=args.xtnt)


