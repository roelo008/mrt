"""
== MULTIRECLASS TOOL ==
Code: Hans Roelofsen
Ideas: Henk Meeuwsen, Bart de Knegt, Hans Roelofsen
Wageningen Environmental Research Institute
18 augustus 2020

Classes and functions in this script are designed to interact with a Combined Raster (combi_raster output
of the ArcGIS Pro Combine Tool https://pro.arcgis.com/en/pro-app/tool-reference/spatial-analyst/combine.htm). The
combined raster has unique integer value assigned to each unique combination of source rasters. Each source raster is
represented by a field in the output raster attribute table (RAT).

There are two components to the interaction with the combined raster
1. UPSTREAM
New values and categories are assigned to specific value-combinations in the source-rasters, based on 'reclass rules'.
This is the Multireclass part and results in 2 new columns to the RAT: newval and newcat

2. DOWNSTREAM
In downstream, newvals are relabeled and/or aggregated to a new set of categories corresponding to the input need of a
certain geo-spatial model. A new column is added to the RAT for each geo-spatial model showing corresponding categories
for each newval.

Upstream reclass rules, as well as downstream reshuffling are user-provided in an Excel-sheet.
Open the combi_raster in ArcGIS pro to see new_cat and geospatial model classifications.

Important terminology

combival:       the values of the combined raster, ie the Value column of the combined raster RAT. The MRT is essence
                just maps combival-values to either a newval, or an outval.
firstrule_cat:  name of the reclass rule w. highest priority for a combival
newval:         the *new value* that is assigned to each pixel in the combined raster as a result of applying the
                reclass rules. Each rule has a `newval`, although > 1 rules may have the same newval.
outval:         integer value in an output raster. Equal to newval when writing the newval raster
cat:            text description of a land use category related to an outval/newval.
model:          refers to an ESD model that uses a land use map but with a different categorization, usually >1 newval
                values are grouped to a model-category. This is defined in the columns in the downstream sheet of the
                input Excel. A model can be written to a geospatial raster with self.write_raster function.
                Default models are `newval` (for writing the result of looking upstream) and `firstrule`
                (for writing an integer identifying the applicably reclass rule in of each pixel).


combival --> Look Upstream --> firstrule_cat  --> Look Downstream --> <model_x>_outcat
                            -> newval_val                                 |
                            -> newval_cat                             <model_x>_out_val
"""

import os
import pandas as pd
import geopandas as gp
import rasterio as rio
import numpy as np
import datetime
import sys
import pathlib
import mrt_helpers as mrt

# from sample import mrt_helpers as mrt

# Assume benb_utils is present on same level als mrt, eg:
# c:\apps\proj_code\mrt\sample\mrt_classes.py << we are here
# c:\apps\proj_code\                          << add this path to sys.path

filepath = pathlib.Path(__file__)
proj_code_dir = str(pathlib.Path(filepath).parents[2])
sys.path.append(proj_code_dir)
from benb_utils.fix_bt import fix_bt
from benb_utils.snl_beheertypen import get_snl_beheertypen_list


class Model:
    """
    Class holding all information related to a Downstream model.
    """

    def __init__(self, name, v):
        self.name = name  # name of the model, ie column in downstream sheet
        self.key = None  # Model looks to either newval or firstrule as key for category
        self.src = None  # are pxl values sourced from Excel or automatically asssigned?
        self.newval2cat = {}  # mapping between the Newval and the output category
        self.rule2cat = {}  # mapping between the reclass rules and output category
        self.cat2outval = {}  # reverse of outval2cat
        self.outval2cat = {}  # mapping between Output value and Category description
        self.str_outval2cat = {}  # as above with values as strings (for GTiff tags)
        self.verbose = v
        if self.verbose:
            print("created model {}".format(self.name))

    def report(self):
        print(
            "I am model {0} with key {1} and pixel value source {2}".format(
                self.name, self.key, self.src
            )
        )


class ReClass:
    """
    Class for reclassing a Combined raster into one or more new categorisations based on reclass rules.
    """

    def __init__(
        self,
        xls_dir,
        xls_in,
        upstream_sheet,
        combined_raster_dir,
        combined_raster_in,
        downstream_sheet,
        verbose,
        ovr,
        base_out=None,
    ):
        """
        Initialisation of the whole thing
        :param xls_dir: directory of steering XLS
        :param xls_in: name and extension of steering XLS
        :param upstream_sheet: sheet name with upstream information
        :param downstream_sheet: sheet name with downstream informatio (default None)
        :param combined_raster_dir: directory containing combined raster
        :param combined_raster_in: name and extension of combined raster.
        """
        if not os.path.isfile(os.path.join(xls_dir, xls_in)):
            raise Exception(
                "  Input file {0} not found in directory {1}".format(xls_in, xls_dir)
            )
        self.__xls = os.path.join(xls_dir, xls_in)
        self.us_sheet = upstream_sheet
        self.ds_sheet = downstream_sheet
        self.models = []
        self.not_classified = False
        self.rules = []
        self.rules2apply = None
        self.rule2newval = None  # dict mapping between Rule Name and Rule Newval
        self.rule2description = (
            None  # dict mapping between Rule Name and Rule Description
        )
        self.not_classified_lst = []
        self.written_rasters = []
        self.verbose = verbose
        self.overviews = ovr  # build overviews or not?
        if os.path.isdir(base_out):
            self.base_out = base_out
        else:
            raise Exception("{} is not a valid output directory".format(base_out))
        self.timestamp_full = datetime.datetime.now().strftime("%d-%b-%Y_%H:%M:%S")
        xls_created = os.path.getctime(self.__xls)
        xls_last_mod = os.path.getmtime(self.__xls)
        self.xls_created_ts = datetime.datetime.fromtimestamp(xls_created).strftime(
            "%Y%m%d-%H%M%S"
        )
        self.xls_created_ts_f = datetime.datetime.fromtimestamp(xls_created).strftime(
            "%d-%b-%Y_%H:%M:%S"
        )
        self.xls_mod_ts = datetime.datetime.fromtimestamp(xls_last_mod).strftime(
            "%Y%m%d-%H%M%S"
        )
        self.xls_mod_ts_f = datetime.datetime.fromtimestamp(xls_last_mod).strftime(
            "%d-%b-%Y_%H:%M:%S"
        )
        self.project_name = "{0}{1}_{2}".format(
            os.path.splitext(xls_in)[0],
            os.path.splitext(xls_in)[1][1:],
            self.xls_mod_ts,
        )

        try:
            self.rules_df = pd.read_excel(self.__xls, sheet_name=self.us_sheet)
        except (ValueError, PermissionError) as e:
            print(
                "Cannot open Sheet {0} in Excel {1}".format(self.us_sheet, self.__xls)
            )
            raise AssertionError

        # Identify source rasters from upstream sheet (rules_df) and parse all other columns to lowercase
        self.source_rasters = [
            x
            for x in list(self.rules_df)
            if x.lower()
            not in [
                "newval",
                "description",
                "remark",
                "reclass",
                "newvalue",
                "new_val",
                "volgnummer",
            ]
            and not x.startswith("_")
        ]
        col_rename_dict = dict(
            zip(
                [
                    x
                    for x in self.rules_df.columns
                    if x not in self.source_rasters and not x.startswith("_")
                ],
                [
                    x.lower()
                    for x in self.rules_df.columns
                    if x not in self.source_rasters and not x.startswith("_")
                ],
            )
        )  # Dit moet vast simpeler kunnen...
        self.rules_df.rename(columns=col_rename_dict, inplace=True)

        # TODO: misschien hier pas de check uitvoeren op Upstream consistentie met mrt.check_xls_upstream?

        # Parse the queries on each source raster into a full query.
        self.parse_rules()

        # Mapping between rule name <-> newval and rule name <-> category
        self.rule2newval = dict(zip(self.rules_df.name, self.rules_df.newval))
        self.rule2description = dict(zip(self.rules_df.name, self.rules_df.description))

        if self.ds_sheet:
            # If downstream sheet is provided as argument, read from Excel.
            try:
                self.downstream_df = pd.read_excel(self.__xls, sheet_name=self.ds_sheet)
            except ValueError as e:
                print("incorrect Excel sheet name: {0}".format(self.ds_sheet))
                raise

            # Downstream models are the columns in the downstream sheet, except for the description and newval columns
            self.models = [
                x
                for x in list(self.downstream_df)
                if x.lower() not in ["description", "newval"]
            ]

            # Verify that all upstream newvals are represented in downstream.
            # missing = set(self.rules_df.newval).difference(set(self.downstream_df.newval))
            # if missing:
            #     print("Warning, upstream newvals {} are not fully represented downstreams" \
            #           .format(', '.join([str(x) for x in missing])))

        if any([x.startswith("_") for x in list(self.rules_df)]):
            # Models based on Rule key instead of newval
            self.models += [x for x in list(self.rules_df) if x.startswith("_")]

        if self.models:
            # Create model objects and attach as attribute for each downstream model
            for model_name in self.models:
                model_obj = Model(name=model_name, v=self.verbose)
                model_obj.key = "firstrule" if model_name.startswith("_") else "newval"

                if model_obj.key == "newval":
                    # Add mapping between newval and model output category
                    output_categories = self.downstream_df.loc[:, model_name].fillna(
                        "geen"
                    )
                    newval2cat = dict(zip(self.downstream_df.newval, output_categories))
                    newval2cat[0] = "geen"  # to catch Rule999
                    model_obj.newval2cat = newval2cat

                elif model_obj.key == "firstrule":
                    # Add mapping between Rule name and Model output category
                    output_categories = self.rules_df.loc[:, model_name].fillna("geen")
                    rule2cat = dict(zip(self.rules_df.name, output_categories))
                    rule2cat[0] = "geen"
                    model_obj.rule2cat = rule2cat
                else:
                    raise AssertionError("scientific progress says Boink")

                # Add mapping between model output categories and output values. First look in Excel for user-defined
                # outvals, else just enumerate.
                try:
                    model_df = pd.read_excel(self.__xls, sheet_name=model_name)
                    u_output_categories = model_df.description
                    outvals = model_df.value
                    val_src = "from xls"

                    # Assert output categories in Excel sheet are identical to categories in downstream
                    diff = set(output_categories).difference(set(u_output_categories))
                    if diff:
                        raise AssertionError(
                            'Mismatch in "{0}" categories between downstream sheet '
                            "and output value sheet: {1}".format(model_name, diff)
                        )

                except (ValueError):
                    # If no seperate sheet is provided, enumerate the unique output categories to generate output values
                    # Make sure to retain order of the original output_categories
                    u_output_categories = (
                        pd.Series(output_categories).drop_duplicates().tolist()
                    )
                    outvals = [i for i, _ in enumerate(u_output_categories, start=1)]
                    val_src = "enumerated"

                # Mapping between model categories and outputvalues. The output value for 'geen' is temporarily set to
                # 0 here, but changed to the requested NoData value later on.
                model_obj.cat2outval = {
                    **dict(zip(u_output_categories, outvals)),
                    **{"geen": 0},
                }

                # Mapping between output values and model categories
                model_obj.outval2cat = dict(zip(outvals, u_output_categories))
                model_obj.str_outval2cat = dict(
                    zip(map(str, outvals), u_output_categories)
                )

                model_obj.src = val_src

                # Set as attribute
                setattr(self, model_name, model_obj)

                if self.verbose:
                    getattr(self, model_name).report()

        # Read combi raster
        self.combi_raster = CombinedRaster(
            combined_raster_dir, combined_raster_in, verbose=self.verbose
        )
        self.reclassed_sheet = (
            True if self.combi_raster.rat.shape[0] < 1048576 else False
        )

        # Check if all source-rasters are adressed in the Excel.
        if len(self.source_rasters) < len(self.combi_raster.source_rasters):
            print("Not all source-rasters are adressed in the Excel: ")
            print(
                "\n".join(
                    set(self.combi_raster.source_rasters).difference(
                        set(self.source_rasters)
                    )
                )
            )

        # Check if col names and values in rules dataframe correspond to provided combined raster
        if not set(self.source_rasters).issubset(set(self.combi_raster.source_rasters)):
            print("Some reclass columns do not match Combined Raster: ")
            print("Columns in the reclass sheet are:")
            print("  \n".join(self.source_rasters))
            print("\nColumns in the combi raster sheet are:")
            print("  \n".join(self.combi_raster.source_rasters))
            raise AssertionError(
                "Please add/remove/rename columns from upstream sheet."
            )

        # Checks for each source raster
        for source_rast in self.source_rasters:
            # the provided (ie asked for) values in a source raster are:
            val_lists = [
                x
                for x in self.rules_df.loc[
                    self.rules_df.reclass == 1, source_rast
                ].apply(mrt.extract_values)
                if x
            ]
            provided_vals = set([item for sublist in val_lists for item in sublist])
            # the valid values are retrieved from the source raster of the combined raster:
            valid_vals = getattr(getattr(self.combi_raster, source_rast), "vals")

            # Are values provided in the queries actually present in the source raster?
            assert provided_vals.issubset(
                valid_vals
            ), "Invalid values found for column {0}: {1}".format(
                source_rast, provided_vals.difference(valid_vals)
            )

            # Are all values in the source raster actually employed in a query?
            if not valid_vals.issubset(provided_vals):
                self.not_classified = True
                valid_not_used = pd.Series(
                    data=list(valid_vals.difference(provided_vals)), name=source_rast
                )
                self.not_classified_lst.append(valid_not_used)

        if self.not_classified and self.verbose:
            print(
                "\nWarning, not all valid values of the source rasters are employed in a Reclass Rule"
            )
            for s in self.not_classified_lst:
                print("{}".format(s.name))
                print("  \n".join([str(x) for x in s]))

    def parse_rules(self):
        """
        Convert the raw information from the rules dataframe to actual queries.
        Note that rule information is stored in both self.rule_df as well as Rule objects stored as attributes and
        in self.rules=[]. It is probably possible to simplify this.
        :return: update self.rule_df and self.rules=[Rule0, Rule1 etc]
        """

        if self.verbose:
            print("Parsing rules")

        # Build pandas dataframe query for each rule. First a query for each source raster as a new col
        temp_cols = []
        for raster in self.source_rasters:
            temp_name = "src_query_{}".format(raster)
            temp_cols.append(temp_name)
            self.rules_df[temp_name] = self.rules_df.loc[:, raster].apply(
                mrt.parse_rule, category=raster
            )
        # Join the piece-queries of each source raster together into a single query.
        self.rules_df["mrt_query"] = self.rules_df.apply(
            lambda row: " and ".join(
                [
                    str(i)
                    for i in row.loc[
                        row.index.str.startswith("src_query_") & row.notna()
                    ]
                ]
            ),
            axis=1,
        )
        self.rules_df.drop(labels=temp_cols, axis=1, inplace=True)

        # Replace queries with zero length, meaning that all source rasters are set to 'n', to generic catch none query
        self.rules_df.loc[
            self.rules_df.mrt_query.str.len() == 0, "mrt_query"
        ] = "index != index"

        # Add dummy rule (Rule999) to catch all values in the combined raster RAT.
        self.rules_df = self.rules_df.append(mrt.rule999(self.source_rasters))

        # which rules are set to 'reclass'
        self.rules_df["reclass"] = np.where(self.rules_df.reclass == 1, True, False)
        self.rules2apply = self.rules_df.loc[self.rules_df.reclass].index

        # Name the rules and set rule priority
        rule_names = ["rule{:03}".format(i) for i in self.rules_df.index]
        self.rules_df["name"] = rule_names
        self.rules_df["priority"] = self.rules_df.index

    def look_upstream(self):
        """
        Apply reclass rules to combi raster VAT.
        :return: updated self.combi_raster.rat with 'first_rule (applicable rule), newval and newcat as new columns
        """

        if self.verbose:
            print("Looking upstream")

        # Holders for gross and net area match for reclass rules
        gross_match_lst = []
        net_match_lst = []

        # New column to combi_raster RAT carrying the FIRST APPLICABLE RECLASS RULE. Default value = Rule999
        self.combi_raster.rat["firstrule_cat"] = "Rule999"

        # Index of combi_raster RAT showing which rules have NOT been assigned a FIRST APPLICABLE RECLASS RULE
        rows_not_assigned = self.combi_raster.rat.index

        # Iterate over reclass rules
        for rule in self.rules_df.itertuples():

            # Identify ALL rows in combi_raster.rat that comply to the reclass rule
            gross_rat_match = self.combi_raster.rat.query(rule.mrt_query).index

            # Keep only those rows that have not yet been assigned
            net_rat_match = gross_rat_match.intersection(rows_not_assigned)

            # Calculate gross match in hectare
            gross_match_ha = np.multiply(
                self.combi_raster.rat.loc[gross_rat_match].Count.sum(),
                self.combi_raster.pixel_area_ha,
            )
            gross_match_lst.append(gross_match_ha)

            if rule.reclass:
                # Assign this rule as the applicable rule only if the rule is set to reclass=True
                # Note if net_rat_match is empty, then nothing will happen here
                self.combi_raster.rat.loc[net_rat_match, "firstrule_cat"] = rule.name

                # Calculate net match
                net_match_ha = np.multiply(
                    self.combi_raster.rat.loc[net_rat_match].Count.sum(),
                    self.combi_raster.pixel_area_ha,
                )
                net_match_lst.append(net_match_ha)

                # Remove net_rat_match rows from unassigned rows
                rows_not_assigned = rows_not_assigned.difference(net_rat_match)
            else:
                net_match_lst.append(0)

            if self.verbose:
                print(
                    "doing rule {0} w query {1} and gross match {2}".format(
                        rule.name, rule.mrt_query, gross_match_ha
                    )
                )

        # Append gross, net and difference to rules dataframe
        self.rules_df["gross_match_ha"] = gross_match_lst
        self.rules_df["net_match_ha"] = net_match_lst
        self.rules_df["difference"] = self.rules_df.gross_match_ha.subtract(
            self.rules_df.net_match_ha
        )

        # Attach newval_val and newval_cat to combi_raster rat
        self.combi_raster.rat["newval_val"] = self.combi_raster.rat.firstrule_cat.map(
            self.rule2newval
        )
        self.combi_raster.rat["newval_cat"] = self.combi_raster.rat.firstrule_cat.map(
            self.rule2description
        )

        # Warning if Rule999 applies to any other combinations than 0, 0, 0
        rule999_matches = self.combi_raster.rat.loc[
            self.combi_raster.rat.firstrule_cat == "rule999"
        ].index
        if len(rule999_matches) >= 1 and self.verbose:
            print(
                "Warning, some categories have not been reclassed into a new category. "
            )

        # Add mapping between newval value --> category and category --> newval value as a Model object named newval
        # Note how 'newval' is considered one of several downstream models to ensure compatability with write_raster
        # Also note how Rule999 is integrated here into the newvals, even thought it is not present in self.us_sheet
        # Also note that these models are limited to: 1) rules that where applied and
        #                                             2) rules with net_match > 0

        non_empty_rules = self.rules_df.loc[self.rules_df.net_match_ha > 0, :].index
        applied_cats = self.rules_df.loc[
            self.rules2apply.intersection(non_empty_rules), "description"
        ]
        applied_vals = self.rules_df.loc[
            self.rules2apply.intersection(non_empty_rules), "newval"
        ]

        model_obj = Model(name="newval", v=self.verbose)
        model_obj.src = "enumerated"
        model_obj.key = "Not Applicable"
        model_obj.cat2outval = dict(zip(applied_cats, applied_vals))
        model_obj.outval2cat = dict(zip(applied_vals, applied_cats))
        model_obj.str_outval2cat = dict(
            zip([str(i) for i in applied_vals], applied_cats)
        )
        model_obj.newval2cat = model_obj.outval2cat
        setattr(self, "newval", model_obj)
        self.models.append("newval")

        if self.verbose:
            model_obj.report()

        del model_obj

        # Add Rule Model class object to map between the Combined Raster value and the applicable rule. Use this
        # to write a raster where each pixel-value points to applicable rule. This is usefull when > 1 reclass rules
        # have the same newval, ie when from the newval alone the applicable rule can no longer be discerned.
        #
        # Rule name equals the Rule Cat, ie column 'firstrule_cat' in self.combi_raster.rat
        # Simple enumeration of the rules are the out_values. These are not equal to the rule priorities, to avoid
        # having the 999 from rule999 as an output value.
        # The newvals are simply the newvals associated with each rule.

        rule_cats = self.rules_df.loc[self.rules2apply, "name"]
        rule_remarks = self.rules_df.loc[self.rules2apply, "remark"]
        out_vals = [i for i, _ in enumerate(rule_cats, start=1)]
        rule_cats_full = rule_cats.str.cat(rule_remarks, sep="_", na_rep="X")

        model_obj = Model(name="rule", v=self.verbose)
        model_obj.src = "enumerated"
        model_obj.key = "Not Applicable"
        model_obj.cat2outval = dict(zip(rule_cats, out_vals))
        model_obj.outval2cat = dict(zip(out_vals, rule_cats_full))
        model_obj.str_outval2cat = dict(
            zip([i for i in map(str, out_vals)], rule_cats_full)
        )
        setattr(self, "firstrule", model_obj)
        self.models.append("firstrule")

        if self.verbose:
            model_obj.report()

        del model_obj

    def look_downstream(self):
        """
        Add model categorisation to the combined raster RAT
        :return: update self.combi_raster.vat with {model}_cat for model in self.models
        """

        assert hasattr(
            self.combi_raster.rat, "newval_val"
        ), "Look upstream before looking downstream"
        assert self.models, "Cannot look downstream..."

        if self.verbose:
            print("Looking downstream")

        for model_name in [
            name for name in self.models if name not in ["newval", "firstrule"]
        ]:

            # Look downstream based on newval or the first applicable reclass rule?
            key = getattr(getattr(self, model_name), "key")
            outcat_colname = "{}_cat".format(model_name)

            if key == "newval":
                newval2cat = getattr(getattr(self, model_name), "newval2cat")
                self.combi_raster.rat.loc[
                    :, outcat_colname
                ] = self.combi_raster.rat.newval_val.map(newval2cat)
            elif key == "firstrule":
                rule2cat = getattr(getattr(self, model_name), "rule2cat")
                self.combi_raster.rat.loc[
                    :, outcat_colname
                ] = self.combi_raster.rat.firstrule_cat.map(rule2cat)
            else:
                raise AssertionError("this isnt happening... ")

            # Update combi raster RAT with model Output value for each Value in the RAT if we are sure that
            # the reclassed sheet will be written to the report.
            if self.reclassed_sheet:
                outval_colname = "{}_val".format(model_name)
                self.combi_raster.rat.loc[
                    :, outval_colname
                ] = self.combi_raster.rat.newval_val.map(
                    getattr(getattr(self, model_name), "cat2outval")
                )

    def write_raster(
        self, request_raster, extent="BNL", out_format="geotiff", nodata=None, **kwargs
    ):
        """
        Write a new raster with pixel values reclass to one of the
        columns of self.combi_raster.rat and optionally cropped to either the LCEU or BNL format
        :param request_raster: name of the RAT attribute table column to which to map pixel values
        :param extent: profile for extent of output raster {BNL, LCEU}. Default: BNL
        :param out_format: geospatial raster file format {geotiff, flt, map}. Default: geotiff
        :param nodata: value in output raster to be assigned as NoData. Default highest value in pixeltype
        :return: TIF file in out_dir
        """

        assert os.path.isdir(self.base_out), "Invalid output directory"
        assert self.models, "nothing to write."
        assert request_raster in self.models, "Invalid request"

        # Translate the Categories of the Requested Raster from the Combi Raster RAT Value column to output values via
        # Combival --> <request_raster>_cat --> <request_raster>_outval

        # Get dictionary to map output categories to output values for requested raster
        cat2outval = getattr(getattr(self, request_raster), "cat2outval")

        # Datatype of output raster is either UInt8 or UInt 16 depending on the range of output values
        out_dtype = mrt.vals2dtype([v for _, v in cat2outval.items()])

        # Use specficied NoData value or else default to max of datatype
        # TODO: use mrt.dtype2nodata()
        out_nodata = (
            nodata
            if nodata is not None
            else {
                "f": lambda x: np.finfo(x).max,
                "i": lambda x: np.iinfo(x).max,
                "u": lambda x: np.iinfo(x).max,
            }[out_dtype.name[0]](out_dtype)
        )

        # Which outcomes of the reclassification should be written as NoData? This is ambigious...
        # Here it is decided that: 1) Rule999 No Other Rule Applies is a **valid** value, so no NoData
        #                          2) 'Geen' from a downstream model is not a **valid** value, so should be NoData
        cat2outval["geen"] = out_nodata

        # Create dictionary between the Combi Raster Values and the Output values, using the <request_raster>_cat
        # column as intermediary
        value2val = dict(
            zip(
                self.combi_raster.rat.Value,
                self.combi_raster.rat.loc[:, "{}_cat".format(request_raster)].map(
                    cat2outval
                ),
            )
        )

        # Add combi_raster Nodata value to the dictionary.
        if self.combi_raster.raster.nodata:
            value2val[self.combi_raster.raster.nodata] = out_nodata

        # Test if top left pixel of combi_raster is a dictionary key. This pixel is assumed to have the
        # combi-raster NoData value (if any) and should map to the requested NoData output value.
        test_pxl = self.combi_raster.raster.read(
            1, window=rio.windows.Window(0, 0, 1, 1)
        )
        try:
            mrt.vec_translate(test_pxl, value2val)
        except KeyError:
            # KeyError occurs when the top-left pixel is not mentioned in the CombiRaster VAT.
            # This happens when it is set to NoData and the Raster VAT does not contain the NoData Value
            raise AssertionError(
                "Top left combi-raster pixel w val {} cannot be reclassed".format(
                    test_pxl.item()
                )
            )

        # Fetch raster profile with extent and pixel size
        prof = mrt.raster_profile(
            extent=extent,
            pxl_size=self.combi_raster.raster.res[0],
            src=self.combi_raster.raster,
        )
        prof.update(
            dtype=out_dtype, nodata=out_nodata, driver=mrt.format2driver(out_format)
        )
        out_name = "{0}_{1}".format(self.project_name, request_raster)

        if self.models:
            # Write DBF to file with Value=Unique values of the request raster and Description=Category Description
            out_vat = gp.GeoDataFrame.from_dict(
                getattr(getattr(self, request_raster), "outval2cat"),
                orient="index",
                columns=["Description"],
            )
            out_vat.loc[:, "Value"] = out_vat.index
            out_vat.loc[:, "geometry"] = None
            out_vat.set_geometry(col="geometry", inplace=True)
            mrt.write_vat(
                gdf=out_vat.sort_values(by="Value"),
                out_dir=self.base_out,
                out_name=out_name,
                v=self.verbose,
            )

            # Write QGIS Layer Style File (QML) to file
            mrt.write_qml(tab=out_vat, out_dir=self.base_out, out_name=out_name)

        # Write to new file, one block at the time. Optionally reduce extent from BNL extent to LCEU extent
        #  BNL  (     0, 300.000, 280.000, 625.000)
        #  LCEU (10.000, 300.000, 280.000, 620.000)
        # Crop top rows: (625.000-620.000)/2.5 = 2.000
        # Crop left cols: (10.000 - 0)/2.5 = 4.000

        destination = os.path.join(
            self.base_out, "{0}.{1}".format(out_name, mrt.format2ext(out_format))
        )
        with rio.open(destination, "w", **prof) as dest:
            dest.update_tags(
                who_am_i=destination,
                creation_date=self.timestamp_full,
                created_by=os.getenv("USERNAME"),
                src_file=self.combi_raster.raster.name,
                content=request_raster,
                remap_xls=self.__xls,
                remap_xls_created=self.xls_created_ts_f,
                remap_xls_modified=self.xls_mod_ts_f,
                spatial_extent=extent,
            )
            # Hard-code all pixel values and corresponding categories into the raster
            dest.update_tags(**getattr(getattr(self, request_raster), "str_outval2cat"))

            # Use native blocks of combi raster for R/W. If cropping to LCEU extent, then these must be a line each.
            # Else, the more common 128x128 shape is allowed.
            src_blocks = [x for x in self.combi_raster.raster.block_windows(1)]
            for block in mrt.progress_bar(
                iterable=src_blocks,
                suffix="Complete",
                length=50,
                prefix="{:<25}".format("Reclass to {0}".format(request_raster)),
            ):

                # In rasterio 0.36 a block is:
                # ((row, col), ((row_start, row_end), (col_start, col_end)))
                # In rasterio 1.1.8 a blocks is:
                # ((row, col), Window(coll_offset, row_offset, width, height)), eg
                # Window(col_off=0, row_off=0, width=112000, height=1). Where Window is a class: rio.windows.Window()
                # See: https://rasterio.readthedocs.io/en/latest/topics/windowed-rw.html#blocks

                # Read the window and get starting and end points of the row and columns
                window = block[1]
                row_start = window.row_off
                row_end = row_start + window.height
                col_start = window.col_off
                col_end = col_start + window.width

                if extent == "LCEU":
                    # check that blocks are equal to 1 row of the combi_raster, which happens to be the case for AGPro
                    # Combine tool outputs, but may or may not be so for other rasters. If they aren't, it will be harder
                    # to apply extent cropping.
                    assert (
                        np.subtract(row_end, row_start) == 1
                    ), "Block shapes are not to expectations"
                    assert (
                        np.subtract(col_end, col_start)
                        == self.combi_raster.raster.shape[1]
                    ), "Block shapes are not to expectations"

                    # Crop from the top the difference in the 'top' extent b'teen LCEU and BNL as pixel count
                    pxl_top_crop = np.divide(
                        np.subtract(625000, 620000), self.combi_raster.raster.res[0]
                    )
                    # Crop from the left the difference in the 'left' extent b'tween LCEU and BNL as pixel count
                    pxl_left_crop = np.divide(
                        np.subtract(10000, 0), self.combi_raster.raster.res[0]
                    )

                if extent == "LCEU" and row_start < pxl_top_crop:
                    continue  # skipping top rows
                elif extent == "LCEU" and row_start >= pxl_top_crop:
                    read_window = rio.windows.Window.from_slices(
                        (row_start, row_end), (pxl_left_crop, col_end)
                    )
                    write_window = rio.windows.Window.from_slices(
                        (row_start - pxl_top_crop, row_end - pxl_top_crop),
                        (0, col_end - pxl_left_crop),
                    )
                else:
                    read_window = window
                    write_window = window

                # Read portion of combi raster
                arr = self.combi_raster.raster.read(1, window=read_window)

                # Reclass according to dictionary
                arr_reclass = mrt.vec_translate(arr, value2val).astype(out_dtype)

                # Write portion to file
                dest.write(arr_reclass, window=write_window, indexes=1)

        if self.overviews:
            # Build overviews if requested. See: https://rasterio.readthedocs.io/en/latest/topics/overviews.html
            dst = rio.open(destination, "r+")
            dst.build_overviews(
                factors=[2, 4, 8, 16], resampling=rio.enums.Resampling.nearest
            )
            dst.update_tags(ns="rio_overview", resampling="average")
            dst.close()

        # assert everything has worked and add to list of written rasters
        if os.path.isfile(destination):
            self.written_rasters.append(destination)

    def mnp_table(self, request_raster="neergeschaalde_beheertypen"):
        """
        write MNP style *.csv file for param request raster.
        See: https://opengis.wur.nl/MNP/contents/data_specificaties.html#land-type-raster-legenda-tabel
        """

        TABLE_COLUMNS = ["land_type_code", "land_type_name", "land_type_id"]

        # Get legend, ie pixel value <-> output category. Add col with MNP style Beheertype Codes Nxx.yy.zz
        legend = pd.DataFrame.from_dict(
            getattr(getattr(self, request_raster), "outval2cat"),
            orient="index",
            columns=[TABLE_COLUMNS[1]],
        )

        legend[TABLE_COLUMNS[2]] = legend.index

        legend.drop(
            legend.loc[
                legend[TABLE_COLUMNS[1]].isin(["geen", "No other rule applies"])
            ].index,
            inplace=True,
            axis=0,
        )

        # Create MNP-style Beheertypecodes using first part of the Category descriptions N17.03 Mooi Park --> N17.03.00
        legend[TABLE_COLUMNS[0]] = (
            legend.loc[:, TABLE_COLUMNS[1]]
            .str.split(" ", n=1, expand=True)[0]
            .apply(fix_bt, as_mnp=True, verbose=self.verbose, strict=False)
        )

        # Write to csv file.
        destination = os.path.join(
            self.base_out, "{0}_{1}.csv".format(self.project_name, request_raster)
        )
        legend.sort_values(by=TABLE_COLUMNS[2]).to_csv(
            destination, index=False, header=True, sep=","
        )

    def write_report(self):
        """
        Write summary area statistics for both source rasters, new_cats, downstream models and rules
        :return: Excel file to file
        """

        assert os.path.isdir(self.base_out), "Invalid output directory"
        excel_out = "{0}_report.xlsx".format(self.project_name)
        with pd.ExcelWriter(os.path.join(self.base_out, excel_out), mode="w") as writer:

            # Report on input
            metadata = {
                "input_XLS": self.__xls,
                "input_XLS_created": self.xls_created_ts_f,
                "input_XLS_last_modified": self.xls_mod_ts_f,
                "input_downstream_sheet": self.ds_sheet,
                "input_upstream_sheet": self.us_sheet,
                "input_combined_raster": self.combi_raster.raster.name,
                "report creation_date": self.timestamp_full,
                "created_by": os.getenv("USERNAME"),
            }
            if len(self.written_rasters) > 0:
                for i, j in enumerate(self.written_rasters, start=1):
                    metadata.update({"output_raster_{}".format(i): j})
            input_df = pd.DataFrame(data=metadata, index=[0])
            input_df.T.to_excel(writer, sheet_name="Metadata", index=True)

            # Write rules to sheet
            self.rules_df.to_excel(
                writer, sheet_name="ReclassRules", index=False, freeze_panes=(1, 2)
            )

            # Write downstream table for applied rules only
            applied_newvals = set(self.rules_df.loc[self.rules2apply, "newval"])
            if self.ds_sheet:
                self.downstream_df.loc[
                    self.downstream_df.newval.isin(applied_newvals), :
                ].to_excel(
                    writer, sheet_name="downstream", index=False, freeze_panes=(1, 2)
                )

            # Report on combi raster RAT
            # if self.combi_raster.rat.shape[0] < 1048576:
            if self.reclassed_sheet:
                col_sel = [
                    column
                    for column in self.combi_raster.rat.columns.to_list()
                    if column != "Geometry"
                ]
                self.combi_raster.rat.loc[:, col_sel].sort_values(
                    by="firstrule_cat"
                ).to_excel(
                    writer, sheet_name="Reclassed", index=False, freeze_panes=(2, 1)
                )

            # Report on values in source raster that are not classified
            if self.not_classified:
                pd.concat(self.not_classified_lst, axis=1).to_excel(
                    writer, sheet_name="NotClassified", index=False, freeze_panes=(2, 1)
                )

            # Report on source rasters
            for src_raster in self.source_rasters:
                stats = pd.pivot_table(
                    data=self.combi_raster.rat,
                    index=src_raster,
                    values="Count",
                    aggfunc="sum",
                )
                stats.rename(columns={"Count": "pixel_count"}, inplace=True)
                stats["hectare"] = stats.pixel_count.multiply(
                    self.combi_raster.pixel_area_ha
                )
                stats.index.name = "Pixel Value {0}".format(src_raster)
                stats.to_excel(
                    writer, sheet_name="source_raster_{0}".format(src_raster)
                )

            # Report on downstream models
            if self.models:
                for model in [
                    x for x in self.models if x not in ["newval", "firstrule"]
                ]:
                    stats = pd.pivot_table(
                        data=self.combi_raster.rat,
                        index="{}_cat".format(model),
                        values="Count",
                        aggfunc="sum",
                    )
                    stats.rename(columns={"Count": "pixel_count"}, inplace=True)
                    stats["hectare"] = stats.pixel_count.multiply(
                        self.combi_raster.pixel_area_ha
                    )
                    # stats['value'] = stats.index.map()
                    stats.index.name = "Category {0}".format(model)
                    stats.to_excel(writer, sheet_name="ESDModel_{0}".format(model))

    def test(self):
        """ "Test integrity of all inputs and report possibilities."""
        print(
            "\n\nReclass objects based on XLS {0} and Combi Raster {1} with {2} rules for the following source rasters:".format(
                self.__xls, self.combi_raster.raster.name, self.rules_df.shape[0]
            )
        )
        for i, rast in enumerate(self.combi_raster.source_rasters, start=1):
            print("  {0}: {1}".format(i, rast))

        if self.models:
            print("\nCan create input for following downstream categories:")
            for i, model in enumerate(self.models + ["newval", "firstrule"], start=1):
                print("  {0}: {1}".format(i, model))

        if self.base_out:
            print(
                "\n{0} {1} a valid output directory".format(
                    self.base_out, "is" if os.path.isdir(self.base_out) else "is not"
                )
            )


class CombinedRaster:
    """
    Class holding all information related to a integer raster + RAT resulting from arcpy.combine
    See: https://desktop.arcgis.com/en/arcmap/10.3/tools/spatial-analyst-toolbox/combine.htm
    """

    def __init__(self, rast_dir, rast_in, verbose):
        """
        Store info related to the raster and read as rasterio object.
        :param rast_dir: directory containing combined raster
        :param rast_in: name and extension of combined raster, should be GDAL format
        """
        assert os.path.isdir(rast_dir), "invalid directory for combined raster"
        assert os.path.isfile(os.path.join(rast_dir, rast_in)) or os.path.isdir(
            os.path.join(rast_dir, rast_in)
        ), "combined raster does not exist"

        if verbose:
            print("Now reading Combined Raster {}".format(rast_in))

        self.rast_dir = rast_dir
        self.basename, self.ext = os.path.splitext(rast_in)
        self.raster = rio.open(os.path.join(rast_dir, rast_in))

        # Try opening the RAT table using one of several possible file extensions
        if os.path.isfile(
            os.path.join(rast_dir, "{0}.tif.vat.dbf".format(self.basename))
        ):
            self.rat_file = os.path.join(
                rast_dir, "{}.tif.vat.dbf".format(self.basename)
            )
            self.rat = gp.read_file(self.rat_file)
        elif os.path.isfile(
            os.path.join(rast_dir, "{0}.tiff.vat.dbf".format(self.basename))
        ):
            self.rat_file = os.path.join(
                rast_dir, "{}.tiff.vat.dbf".format(self.basename)
            )
            self.rat = gp.read_file(self.rat_file)
        elif os.path.isfile(os.path.join(rast_dir, "{0}.csv".format(self.basename))):
            self.rat_file = os.path.join(rast_dir, "{}.csv".format(self.basename))
            self.rat = pd.read_csv(self.rat_file, sep=",")
        else:
            raise AssertionError(
                "Raster Attribute Table not found. Expected: {0}.csv or {0}.tif.vat.dbf".format(
                    self.basename
                )
            )

        try:
            col_rename_dict = dict(
                zip(
                    [
                        x
                        for x in self.rat.columns
                        if x.lower() in ["value", "count", "geometry"]
                    ],
                    [
                        x.capitalize()
                        for x in self.rat.columns
                        if x.lower() in ["value", "count", "geometry"]
                    ],
                )
            )
            self.rat.rename(columns=col_rename_dict, inplace=True, errors="raise")
        except KeyError:
            raise AssertionError("Unexpected columns in raster attribute table")

        self.source_rasters = [
            x for x in list(self.rat) if x not in ["Value", "Count", "Geometry"]
        ]
        self.pxl_area_m2 = np.square(self.raster.res[0])
        self.pixel_area_ha = np.divide(self.pxl_area_m2, 10000)
        self.negative_vals_found = False

        if verbose:
            print("starting looking at source rasters.")
        for source_raster in self.source_rasters:
            setattr(
                self,
                source_raster,
                SourceRaster(
                    name=source_raster, vals=set(self.rat.loc[:, source_raster])
                ),
            )
            if verbose:
                print(
                    f'added source raster {getattr(getattr(self, source_raster), "name")}'
                )

        # Verify that every cell in the raster is accounted for in the table.
        # assert self.rat.Count.sum() == np.multiply(self.raster.shape[0], self.raster.shape[1]), 'huh?'

        # Check for < 0 values in the Count field, which should not be possible, but appears to happen with AGPro,
        # esp for 2.5 m rasters. Probably due to integer overflow of the Long datatype in the VAT Count field.
        # This assumes that each pixel in the raster is represented in the raster RAT, which MAY NOT BE THE CASE
        # when AGPro NoData masks are present.

        if any(self.rat.Count < 0):
            neg_index = self.rat.loc[self.rat.Count < 0].index
            if len(neg_index) == 1:

                self.negative_vals_found = True
                total_count = (
                    self.raster.shape[0] * self.raster.shape[1]
                )  # total nr of pixels in raster
                accounted_for = self.rat.loc[
                    self.rat.index.difference(neg_index)
                ].Count.sum()  # Pixel count of RAT vals w Count > 0
                negative_count = self.rat.at[
                    neg_index.tolist()[0], "Count"
                ]  # Given negative value, incorrect
                should_be = np.subtract(
                    total_count, accounted_for
                )  # substitue value = total_count - accounted_for

                if verbose:
                    print(
                        "  !Warning, negative value found in raster VAT. Substituting {0} for {1}".format(
                            negative_count, should_be
                        )
                    )

                # update the RAT and also record change
                self.rat.loc[neg_index, "Count"] = should_be
                self.rat.loc[:, "Count_orig"] = self.rat.Count
                self.rat.loc[neg_index, "Count_orig"] = negative_count
                assert self.rat.Count.sum() == total_count
            else:
                raise Exception("Multiple negative values detected in raster VAT.")


class SourceRaster:
    """
    Class for holding all information of a source raster incorporated into a CombinedRaster, as derived from the
    CombinedRaster RAT
    """

    def __init__(self, name, vals):
        self.name = name
        self.vals = vals


if __name__ == "__main__":
    bdb = ReClass(
        xls_dir=r"c:\Users\roelo008\Wageningen University & Research\Natuurverkenning breder doelbereik - Scenariokaart\BDB_beslisregels\tijdelijk",
        xls_in="bdb2050_t10.xlsx",
        upstream_sheet="upstream_250cm",
        downstream_sheet=None,
        combined_raster_dir=r"w:\PROJECTS\Nvk_bdb\a_geodata\b_2050\b_gecombineerd\combi_v9",
        combined_raster_in="bdb_t9_combi.tif",
        base_out=r"c:\apps\temp_geodata\bdb_test\out",
        verbose=True,
    )
    bdb.look_upstream()
    bdb.look_downstream()
    bdb.write_report()
