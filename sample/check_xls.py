import sys
import os
import argparse
import mrt_helpers as mrt

parser = argparse.ArgumentParser()
parser.add_argument('xls', help='path and filename of the reclass rule XLS')
parser.add_argument('--us', help='upstream sheet name')
parser.add_argument('--ds', help='upstream sheet name')
args = parser.parse_args()

xls_dir = os.path.dirname(args.xls)
xls_name = os.path.basename(args.xls)

if args.us:
    status, msg = mrt.verify_newval_description_pairing(xls_dir, xls_name, args.us)
    print('\nChecking {0}, sheet {1}: {2}'.format(xls_name, args.us, 'Passed' if status else 'Failed'))
    print(msg)

if args.ds and args.us:
    status, msg = mrt.check_xls_downstream(xls_dir=xls_dir, xls_name=xls_name, us=args.us, ds=args.ds)
    print('\nChecking {0}, sheet {1}: {2}'.format(xls_name, args.ds, 'Passed' if status else 'Failed'))
    print(msg)

else:
    sys.exit(0)
